var config = {
      realm: 'myrealm',
      ldap : {
        host : "ipa.demo1.freeipa.org",
        port : 389, 
        baseDN : "cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org",
        queryTemplate : "uid={username}"
      }
    };

module.exports = config;
