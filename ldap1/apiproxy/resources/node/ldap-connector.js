// ldap-connector.js
// ------------------------------------------------------------------
//
// Bridge into LDAP, from Apigee Edge.
//
// created: Wed Jun 22 18:45:52 2016
// last saved: <2016-July-13 16:56:57>

var ldap = require("ldapjs"),
    basicAuth = require('basic-auth'),
    q = require('q'),
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express();

var config = require('./config/config.js');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

function unauthorized(response) {
  response.set('WWW-Authenticate', 'Basic realm="' + config.realm + '"');
  return response.status(401).end();
}

function parseBasicAuth (req, res, next) {
  var user = basicAuth(req);
  if (!user || !user.name || !user.pass) { unauthorized(res); }
  req.userCredentials = user;
  return next();
}


function authenticateUser(ctx) {
  var deferred = q.defer(),
      re1 = new RegExp('\\{username\\}'),
      ldapConfig = config.ldap || {},
      baseDN = ldapConfig.baseDN || 'n/a',
      template = ldapConfig.queryTemplate || "uid={username}",
      partialDN = template.replace(re1, ctx.credentials.name),
      fullDN = partialDN + ',' + baseDN,
      ldapPort = config.ldap.port || 389,
      ldapUrl = 'ldap://' + config.ldap.host + ':' + ldapPort,
      ldapClient = ldap.createClient({
        url: ldapUrl
      });

  ldapClient.bind(fullDN, ctx.credentials.pass, function(error) {
    if (error) {
      ctx.loginStatus = 401;
      ldapClient.unbind();
      deferred.resolve(ctx);
      return;
    }

    ldapClient.search(fullDN, { scope: 'base' }, function(error, res){
      res.on('searchEntry', function(entry) {
        ctx.userInfo = entry.object;
      });
      res.on('searchReference', function(referral) {
        // console.log('referral: ' + referral.uris.join());
      });
      res.on('error', function(error) {
        console.error('error: ' + error.message);
        ctx.loginStatus = 0;
        ctx.error = error;
        deferred.resolve(ctx);
      });
      res.on('end', function(result) {
        ctx.loginStatus = 200;
        ctx.result = result;
        ldapClient.unbind();
        deferred.resolve(ctx);
      });
    });

  });

  return deferred.promise;
}

function newGuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
}

function logError(e, guid) {
  console.log('request id: %s', guid);
  console.log('unhandled error: ' + e);
  console.log(e.stack);
}


app.get('/authn', parseBasicAuth, function(request, response) {
  function renderResponse (ctx) {
      response.header('Content-Type', 'application/json');
      if (ctx.loginStatus == 200) {
        response.status(200).send(ctx.userInfo);
      }
      else {
        unauthorized(response);
      }
  }

  q({ credentials: request.userCredentials })
    .then(authenticateUser)
    .then(renderResponse)
    .done(function() {}, function(e) {
      var guid = newGuid();
      logError(e, guid);
      response.status(500).send({ message : "Server error", id: guid });
    });

});


// default behavior
app.all(/^\/.*/, function(request, response) {
  response.header('Content-Type', 'application/json');
  response.status(404)
    .send('{ "message" : "This is not the server you\'re looking for." }\n');
});


var port = process.env.PORT || 5950;
app.listen(port, function() {
  console.log('listening on port %d...', port);
});
