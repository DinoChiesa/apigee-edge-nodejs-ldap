# Apigee Edge Proxy for LDAP

This is an Apigee Edge API Proxy that connects to a remote LDAP Server via the npm module [ldapjs](https://www.npmjs.com/package/ldapjs).

This demonstrates an easy way to produce APIs that bind and search an LDAP-accessible Directory.

Please note: running this code in the Apigee Edge public cloud means the Apigee
Message Processor needs to be able to connect directly to the LDAP
server, over the public internet.  


## Motivation

Apigee Edge has an [LDAP policy](http://docs.apigee.com/api-services/reference/ldap-policy) that allows an API proxy to perform binds and searches on an LDAP-accessible directory. This policy is available for use only in customer-managed installations of Apigee Edge, not in the Apigee-managed Edge cloud. Which means, in the public cloud, people cannot do binds or searches on any LDAP database.

I suppose the thinking was that it would be  rare for a company to expose
their LDAP server on the internet, so the LDAP policy wouldn't be helpful in the public cloud. 

As my friend Ozan points out, the assumption that LDAP is not used on the public network, is probably not valid.  There are LDAP proxies, LDAP proxies with firewalls, and read-only DC products available, to allow organizations to share a certain part of the directory with limited functions to external applications. In other words, various ways to project LDAP on the DMZ while hiding the central repo under inner networks.

With an LDAP proxy, the setup is not too different than Apigee on-premises accessing IDM in corporate network.  This API proxy demonstrates how to connect to such LDAP resources, without using the LDAP policy.

It might be useful for example, for implementing OAuth2.0 password-grant token issuance, using an LDAP directory as the IdP. 



## How it works

The API Proxy relies on the nodejs support within Apigee Edge. Users do not need to provision a server to run nodejs; the nodejs logic runs inside Apigee Edge within the context of an API Proxy.

## API Endpoints Exposed

There is just one resource available in this API Proxy

```
GET /authn
```

It accepts an Authorization header, containing user credentials formatted according to the HTTP Basic Authentication standard.  

Upon a successful authentication, the API returns 200 along with metadata about the authenticated user.

Upon unsuccessful authentication, the API returns 401, and a WWW-Authenticate challenge. 



## To use the proxy

You must import the API proxy into Apigee Edge, then deploy it. You can use tools such as
[apigeetool](https://github.com/apigee/apigeetool-node) or [pushapi](https://github.com/carloseberhardt/apiploy/blob/master/pushapi) for that purpose. The proxy uses a basepath of /ldap1 .

Therefore, once deployed, the proxy will be available at:

```
GET https://ORGNAME-ENVNAME.apigee.net/ldap1/authn
```

A successful invocation in curl looks like this:

```
curl -i -u employee:Secret123 https://ORGNAME-ENVNAME.apigee.net/ldap1/authn
```

In the above, you must replace `ORGNAME` with the organization name, and `ENVNAME` with the environment name. 

## Connection information

The information needed to connect to a LDAP database includes the server name or IP address, the port, and the base DN. You can also optionally specify the template used for the search. 
The configuration looks like this:

```js
var config = {
      realm: 'myrealm',
      ldap : {
        host : "ipa.demo1.freeipa.org",
        port : 389, 
        baseDN : "cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org",
        queryTemplate : "uid={username}"
      }
    };
```

The port is optional and defaults to 389.  The queryTemplate is optional and defaults to
"uid={username}".  This is a template that gets "{username}" replaced with the passed-in-username, and then it is prepended to the base DN for the purpose of the LDAP bind. For example, using the above configuration, if a request passed in user=joseph , then the full DN would be:

```
uid=joseph,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org
```

The default configuration connects to a free LDAP server available on the internet.  ([demo1.freeipa.org](https://www.freeipa.org/page/Demo)).  To change this, 
you need to specify different settings in the config/config.js file. 


To use the demo1.freeipa.org in demonstrations, you can use these accounts:

| username  | password |
|-----------|----------|
| employee  | Secret123|
| admin     | Secret123|
| manager   | Secret123|


## License

This project is open source, licensed under [the Apache 2.0 License](./LICENSE).

## Extending or Modifying

Users can modify the nodejs code to do whatever it is they want to do, to connect to their directory server.

One possible change people might want to make: when using the nodejs code not as an endpoint in itself, a failed bind due to "No Such Object" probably would not result in a WWW-Authenticate challenge. Instead the proxy might return something else.


## Bugs

1. There's no limit enforced on the number of failed bind attempts, from a single IP, or for a single user. This is left as an exercise for the reader. 

